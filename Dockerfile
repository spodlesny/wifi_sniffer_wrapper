FROM spodlesny/kali_arm

RUN apt-get update
RUN apt-get install -y python-dev python-pip git net-tools inetutils-ping libboost-dev libboost-regex-dev libpcap-dev openssl nano tcpdump aircrack-ng cmake libssl-dev screen
RUN pip install scapy==2.3.1 wsgiref==0.1.2

RUN git clone https://github.com/mfontanini/libtins.git
WORKDIR libtins/
RUN git checkout tags/v3.2
RUN mkdir build
WORKDIR build/

RUN cmake ../ -DLIBTINS_ENABLE_CXX11=1
RUN make
RUN make install

WORKDIR ../../
RUN git clone https://github.com/mfontanini/dot11decrypt.git
WORKDIR dot11decrypt
RUN mkdir build
WORKDIR build
RUN cmake ..
RUN make

WORKDIR ../../
RUN ls
RUN git clone https://github.com/DanMcInerney/net-creds.git

ENV WIFI_INTERFACE=wlan1
ENV WIFI_ENC_TYPE=wpa
ENV WIFI_NAME=test
ENV WIFI_PASSWORD=NBUsr123
ENV WIFI_CHANNEL=1

CMD airmon-ng stop $WIFI_INTERFACE'mon' > /dev/null && airmon-ng start $WIFI_INTERFACE > /dev/null && screen -d -m airodump-ng $WIFI_INTERFACE'mon' --channel $WIFI_CHANNEL && screen -d -m dot11decrypt/build/dot11decrypt $WIFI_INTERFACE'mon' $WIFI_ENC_TYPE:$WIFI_NAME:$WIFI_PASSWORD && sleep 10 && echo "Starting net-creds.py ($(date))" && python -u net-creds/net-creds.py -i tap0
